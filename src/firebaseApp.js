import firebase from "firebase/app";
import "firebase/database";

var config = {
  apiKey: "AIzaSyCiOJlmlnRypaFQ11OgOxH4vP2oCgtq12g",
  authDomain: "britecore-45eee.firebaseapp.com",
  databaseURL: "https://britecore-45eee.firebaseio.com",
  projectId: "britecore-45eee",
  storageBucket: "britecore-45eee.appspot.com",
  messagingSenderId: "117751441482"
};
firebase.initializeApp(config);

const database = firebase.database();

export default database;
