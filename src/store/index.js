import Vue from 'vue'
import Vuex from "vuex";
import database from "../firebaseApp";

Vue.use(Vuex);

const notesRef = database.ref("rows");

const store = new Vuex.Store({
  state: {
    data: []
  },
  mutations: {
    getData(state, data) {
      state.data = data.data;
    }
  },
  actions: {
    getData(context) {
      return new Promise((resolve, reject) => {
        let data = [];
        notesRef.once("value", notes => {
          notes.forEach(note => {
            data.push({
              id: note.child("ID").val(),
              name: note.child("Name").val(),
              amount: parseFloat(note.child("Amount").val()),
              date: note.child("Date").val(),
              description: note.child("Description").val(),
              key: parseInt(note.ref.key)
            });
            // note.child("Amount").val();
          });
          context.commit({
            type: 'getData',
            data: data
          });
          resolve();
        });
      })
    },
    updateRow({
      commit
    }, obj) {
      return new Promise((resolve, reject) => {
        notesRef.child(obj.key).update(obj.data);
        // commit('')
        commit({
          type: 'getData',
          data: obj.fullData
        });
        resolve();
      });
    },
    deleteRow({
      commit
    }, obj) {
      return new Promise((resolve, reject) => {
        notesRef.child(obj.key).remove();
        commit({
          type: 'getData',
          data: obj.data
        });
        // commit('')
        resolve();
      });
    }
  }
})

export default store;
